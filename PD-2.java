////ZAD1A
import java.util.*;
public class Zad1a {
    public static void main(String[] args){
        int x;
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n: ");
        int n = in.nextInt();
        int suma = 0;
        for(int i = 0; i<n ; i++){

            x = in.nextInt();
            suma+=x;


        }
        System.out.println("Suma wynosi: " +suma);


    }
}


////ZAD1B


import java.util.*;
public class Zad1b {
    public static void main(String[] args){
        int x;
        Scanner in = new Scanner(System.in);
        int suma = 1;
        System.out.println("Wprowadź n: ");
        int n = in.nextInt();
        for(int i = 0; i<n ; i++){
            x = in.nextInt();
            suma*=x;
        }
        System.out.println(suma);

    }
}


////ZAD1C
import java.util.*;
import static java.lang.Math.*;
public class Zad1c {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int suma = 0 ;
        for(int i = 0 ; i<n ; i++){
            int x = in.nextInt();
            suma+=abs(x);
        }
        System.out.println(suma);
    }
}

////ZAD1D
import java.util.*;
import static java.lang.Math.*;
public class Zad1d {
    public static void main(String[] args){
        int suma = 0 ;
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n : ");
        int n = in.nextInt();
        for(int i = 0;i<n; i++){
            int x = in.nextInt();
            suma +=sqrt(abs(x));
        }
        System.out.println(suma);



    }
}


////ZAD1E
import java.util.*;
import static java.lang.Math.*;
public class Zad1e {
    public static void main(String[] args){
        int suma = 1 ;
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n : ");
        int n = in.nextInt();
        for(int i = 0;i<n; i++){
            int x = in.nextInt();
            suma *=abs(x);
        }
        System.out.println(suma);



    }
}


////ZAD1F
import java.util.*;
import static java.lang.Math.*;
public class Zad1f {
    public static void main(String[] args){
        int suma = 0 ;
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n : ");
        int n = in.nextInt();
        for(int i = 0;i<n; i++){
            int x = in.nextInt();
            suma +=pow(x,2);
        }
        System.out.println(suma);



    }
}


////ZAD1G


import java.util.*;
import static java.lang.Math.*;
public class Zad1g {
    public static void main(String[] args){
        int suma1 = 0 ;
        int suma2 = 1;
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n : ");
        int n = in.nextInt();
        for(int i = 0;i<n; i++){
            int x = in.nextInt();
            suma1 +=x;
            suma2 *=x;
        }
        System.out.println(suma1);
        System.out.println(suma2);



    }
}




ZAD 2  



import java.util.*;
import java.awt.*;
import java.util.ArrayList;

public class Zad2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        ArrayList lista = new ArrayList();
        for(int i = 0 ; i < n; i++){
            int x = in.nextInt();
            lista.add(x);

        }
//        obiekt = lista.get(0);
//        lista.remove(0);
//        lista.add(obiekt);
        System.out.println(lista);
        lista.add(lista.remove(0));
        System.out.println("Lista po zmianie: ");
        System.out.println(lista);


    }
}


///ZAD2_1a

import java.util.*;

public class Zad2_1a {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n: ");
        int n = in.nextInt();
        int suma = 0;
        for (int i = 0; i<n ; i++){
            int x = in.nextInt();
            if(x%2==1){
                suma+=1;
            }
        }
        System.out.println("Wszystkich liczb nieparzystych jest: " + suma);



    }
}


////ZAD2_1b

import java.util.*;

public class Zad2_1b {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n: ");
        int n = in.nextInt();
        int suma = 0;
        for (int i = 0; i<n ; i++){
            int x = in.nextInt();
            if(x%3==0 && x%5!=0){
                suma+=1;
            }
        }
        System.out.println("Wszystkich liczb podzielnych przez 3 i niepodzielnych przez 5  jest: " + suma);



    }
}


////ZAD2_2


import java.util.*;
public class ZAD2_2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadź n : " );
        int n = in.nextInt();
        int suma = 0;
        for(int i = 0 ;i < n ;i++)
        {
            int x = in.nextInt();
            if(x>0){
                suma+=x;
            }
        }
        System.out.println("Podwojona suma wynosi:" + suma*2);
    }
}


////ZAD 2_3


import java.util.*;
public class ZAD2_3 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Wprowadz liczbe n: ");
        int n = in.nextInt();
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0 ;
        for(int i = 0 ; i < n; i++) {
            int x = in.nextInt();
            if(x>0){
                dodatnie+=1;
            }
            else if(x<0){
                ujemne+=1;
            }
            else{
                zera+=1;
            }
        }
        System.out.println("Liczb dodatnich jest: " + dodatnie);
        System.out.println("Liczb ujemych jest: " + ujemne);
        System.out.println("Zer jest: " + zera);
    }
}


/////ZAD 2_4



import java.util.*;
import java.awt.*;
import java.util.ArrayList;
import static java.lang.Math.*;
public class ZAD2_4 {
    public static void main(String[] args) {
        ArrayList lista = new ArrayList();
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj n: ");

        int smallest = lista.indexOf(0);

        int biggest = lista.indexOf(0);

        int n = in.nextInt();

        for (int i = 0; i < n; i++) {
            int x = in.nextInt();
            lista.add(x);

        }
        for (int o = 1; o < lista.size(); o++) {
            if (lista[o] > biggest)
                biggest = lista[o];
            else if (lista[o] < smallest)
                smallest = lista[o];
        }
        System.out.println(lista);
        System.out.println(smallest);
        System.out.println(biggest);

    }
}



